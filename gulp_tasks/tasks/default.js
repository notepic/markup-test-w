var gulp = require('gulp');
var	runSequence = require('run-sequence');
var config = require('../config');

var isLocal = config.env.isLocal;

gulp.task('default', function() {
	if(isLocal) {
		return runSequence(
			'concat',
			['jade', 'sass'],
			'browserSync',
			'watch'
		);
	} else {
		return runSequence(
			'concat',
			['jade', 'sass'],
			'watch'
		);
	}
});
